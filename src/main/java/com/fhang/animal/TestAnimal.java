/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal" + (h1 instanceof Animal));
        System.out.println("h1 is Landanimal" + (h1 instanceof LandAnimal));
        System.out.println("");

        Cat c1 = new Cat("Obok");
        c1.eat();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal" + (c1 instanceof Animal));
        System.out.println("c1 is Landanimal" + (c1 instanceof LandAnimal));
        System.out.println("");

        Dog d1 = new Dog("Bo");
        d1.eat();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal" + (d1 instanceof Animal));
        System.out.println("d1 is Landanimal" + (d1 instanceof LandAnimal));
        System.out.println("");

        Crocodile cc1 = new Crocodile("Coco");
        cc1.eat();
        cc1.crawl();
        cc1.speak();
        cc1.sleep();
        System.out.println("cc1 is animal" + (cc1 instanceof Animal));
        System.out.println("cc1 is Reptile" + (cc1 instanceof Reptile));
        System.out.println("");

        Snake s1 = new Snake("zaza");
        s1.crawl();
        s1.eat();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal" + (s1 instanceof Animal));
        System.out.println("s1 is Reptile" + (s1 instanceof Reptile));
        System.out.println("");

        Fish f1 = new Fish("wit");
        f1.swim();
        f1.eat();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal" + (f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal" + (f1 instanceof AquaticAnimal));
        System.out.println("");

        Crab cb1 = new Crab("Priya");
        cb1.swim();
        cb1.eat();
        cb1.speak();
        cb1.sleep();
        System.out.println("cb1 is animal" + (cb1 instanceof Animal));
        System.out.println("cb1 is AquaticAnimal" + (cb1 instanceof AquaticAnimal));
        System.out.println("");

        Bat b1 = new Bat("Bubu 19");
        b1.eat();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal" + (b1 instanceof Animal));
        System.out.println("b1 is Poultry" + (b1 instanceof Poultry));
        System.out.println("");

        Bird bd1 = new Bird("Thongchai");
        bd1.eat();
        bd1.fly();
        bd1.speak();
        bd1.sleep();
        System.out.println("bd1 is animal" + (bd1 instanceof Animal));
        System.out.println("bd1 is Poultry" + (bd1 instanceof Poultry));
        System.out.println("");
    }
}
