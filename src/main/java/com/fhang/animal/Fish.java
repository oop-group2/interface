/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class Fish extends AquaticAnimal{
    private String nickname;
    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Fish " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish " + nickname + " eat");
    }

    @Override
    public void speak() {
       System.out.println("Fish " + nickname + " speak pong pong");
    }

    @Override
    public void sleep() {
       System.out.println("Fish " + nickname + " sleep");
    }
    
}
