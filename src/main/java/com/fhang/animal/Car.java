/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class Car extends Vahicle implements Runable{

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car startEngine");
    }

    @Override
    public void stopEngine() {
          System.out.println("Car stoptEngine");
    }

    @Override
    public void raiseSpeed() {
       System.out.println("Car raiseSpeed");
    }

    @Override
    public void applyBreak() {
         System.out.println("Car applyBreak");
    }

    @Override
    public void run() {
         System.out.println("Car run");
    }
    
}
