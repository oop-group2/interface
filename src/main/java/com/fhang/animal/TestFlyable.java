/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.animal;

/**
 *
 * @author Natthakritta
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("BoBo");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog d1 = new Dog("MoMo");
        d1.run();
        Car car = new Car("Engine number I");
        Human h1 = new Human("Fhang");
        Cat c1 = new Cat("dabok");
        Bird bd1 = new Bird("Jeab");

        System.out.println("bat is Flyable : " + (bat instanceof Flyable));
        System.out.println("plane is Flyable : " + (plane instanceof Flyable));

        System.out.println("");
        System.out.println("Flyable");
        Flyable[] flyables = {bat,bd1, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.stopEngine();

            }
        }

        System.out.println("");
        System.out.println("Runable");
        Runable[] runables = {d1, plane, car, h1, c1};
        for (Runable r : runables) {
            r.run();
        }

    }
}
